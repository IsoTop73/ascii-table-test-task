<?php

$data = array (
    array (
        'Name' => 'Trixie',
        'Color' => 'Green',
        'Element' => 'Earth',
        'Likes' => 'Flowers'),
    array (
        'Name' => 'Tinkerbell',
        'Element' => 'Air',
        'Likes' => 'Singning',
        'Color' => 'Blue'),
    array (
        'Name' => 'Blum',
        'Element' => 'Water',
        'Likes' => 'Dancing',
        'Name' => 'Blum',
        'Color' => 'Pink'),
);

class Ascii_table
{
    private $data;
    private $keys;
    private $col_width;
    private $table_width;
    private $divider = '';
    private $cellPadding = 2;

    public function __construct($data) {
        $this->data = $data;
        $this->keys = array_keys($data[0]);
    }

    private function getColWidths() {
        foreach ($this->keys as $key) {
            foreach ($this->data as $elem) {
                $col_data[] = strlen($elem[$key]);
            }

            $col_data[] = strlen($key);

            $this->col_width[$key] = max($col_data) + $this->cellPadding * 2;

            unset($col_data);
        }
    }

    private function getTableWidth() {
        $this->table_width = array_sum($this->col_width);
        $this->table_width += count($this->keys) + 1;
    }

    private function makeDivider() {
        $this->divider = str_repeat('-', $this->table_width);
        $this->divider[0] = '+';

        foreach ($this->keys as $key) {
            static $i = 0;

            $i += $this->col_width[$key] + 1;
            $this->divider[$i] = '+';
        }

        $this->divider .= "\n";
    }

    private function makeRow($array, $keys) {
        $row = '';
        $row .= $this->divider;

        foreach ($keys as $key) {
            $text = $array[$key];
            $cell = '|';

            $cell .= str_repeat(' ', $this->cellPadding) . $text;

            if (is_int($key)) {
                $cell .= str_repeat(' ', $this->col_width[$array[$key]] - strlen($cell) + 1);
            } else {
                $cell .= str_repeat(' ', $this->col_width[$key] - strlen($cell) + 1);
            }

            $row .= $cell;
        }

        $row .= "|\n";

        return $row;
    }

    private function makeBody() {
        $out = '';

        foreach ($this->data as $elem) {
            $out .= $this->makeRow($elem, $this->keys);
        }

        $out .= $this->divider;

        return $out;
    }

    private function makeHeader() {
        return $this->makeRow($this->keys, array_keys($this->keys));
    }

    public function make() {
        $this->getColWidths();
        $this->getTableWidth();
        $this->makeDivider();

        return $this->makeHeader() . $this->makeBody();
    }

    public function print() {
        echo $this->make();
    }
}

$table = new Ascii_table($data);
$table->print();
